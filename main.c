#include <stdio.h>
#include <unistd.h>

#include <directfb.h>

#include <signal.h>
#include "ipc.h"

#include "time.h"

#include "screens.h"
int ipc_fd;


typedef enum {
    SCREEN_READY,
    SCREEN_OPEN,
    SCREEN_DECLINED,
    SCREEN_NETERROR,
    SCREEN_DOORALERT,
    SCREEN_SECURITYBREACH,
    SCREEN_EXIT,
    SCREEN_NOTOURCARD,
	SCREEN_NOTCONFIGURED
} screenId;

screenId currentScreen;



int main(int argc, char **argv) {

    DFBResult res = DirectFBInit(&argc, &argv);
    if (res != DFB_OK) {
        fprintf(stderr,"Could not init DirectFB, exiting\n");
        return -1;
    }
    
    if (open_ipc() == -1) {
        return -1;
    }

    setupDirectFB();
  
    eventId event;
    char cardUid[6];
    drawReadyScreen();
    currentScreen = SCREEN_READY;

    int pollResult = 0;
    while ((pollResult = get_next_event(&event, &cardUid[0])) >= 0) {
        if (event == DOOR_CLOSED || pollResult == 0 && currentScreen != SCREEN_OPEN && currentScreen != SCREEN_EXIT) {
            drawReadyScreen();
            currentScreen = SCREEN_READY;
        } else if (event == DOOR_OPEN) {
            drawOpenScreen();
            currentScreen = SCREEN_OPEN;
        } else if (event == CARD_DECLINED) {
            drawDeclinedScreen(&cardUid[0]);
            currentScreen = SCREEN_DECLINED;
        } else if (event == NETWORK_ERROR) {
            drawNetworkError();
            currentScreen = SCREEN_NETERROR;
        } else if (event == DOOR_ALERT) {
            drawDoorAlert();
            currentScreen = SCREEN_DOORALERT;
        } else if (event == SECURITY_BREACH) {
            drawSecurityBreach();
            currentScreen = SCREEN_SECURITYBREACH;
        } else if (event == DOOR_OPEN_EXIT) {
            drawExitScreen();
            currentScreen = SCREEN_EXIT;
        } else if (event == NOT_OUR_CARD) {
            drawCardNotRecognized();
            currentScreen = SCREEN_NOTOURCARD;
        } else if (event == NOT_CONFIGURED) {
			drawNotConfiguredError();
			currentScreen = SCREEN_NOTCONFIGURED;
		}
    }
    
    releaseDirectFB();
    return 0;
}
