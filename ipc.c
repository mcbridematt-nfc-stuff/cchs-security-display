
#include "ipc.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/select.h>

#include <sys/time.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>

int ipc_fd;

int open_ipc() {
    /* The O_RDWR is necessary so the FIFO isn't closed when
     * there are no writers
     * (see http://www.outflux.net/blog/archives/2008/03/09/using-select-on-a-fifo/ 
     *  for more info)
     */
    ipc_fd = open("/tmp/nfc-frontend.sock",O_RDWR | O_NONBLOCK);
    if (ipc_fd == -1) {
        fprintf(stderr,"No IPC Socket - exiting\n");
        return -1;
    }
    return 0;
}

int get_next_event(eventId *evtId,char *uid) {
    printf("Tick\n");
    if (ipc_fd == -1) {
        return -1;
    }
    fd_set rd,wr,ex;
    FD_ZERO(&rd);
    FD_ZERO(&wr);
    FD_ZERO(&ex);
    FD_SET(ipc_fd,&rd);
    
    struct timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
    
    int fds_remaining = select(ipc_fd+1,&rd,&wr,&ex,&timeout);
    if (fds_remaining == 0) {
        return 0;
    }
    ipc_message msg;
    // Read an event
    int read_bytes = read(ipc_fd,&msg,sizeof(ipc_message));
    if (read_bytes < 0) {
        char *reason = strerror(errno);
        printf("Error reading FIFO (%d,%s)\n",read_bytes,reason);
        
        return -1;
    } else {
        strncpy(uid,msg.cardUid,8);
        uid[8] = '\0';
        *evtId = (eventId)msg.event;
    }
    return 1;
    
}