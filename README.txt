Display for NFC Security system
---------------------------------------

This is a display interface for the NFC security system. It uses DirectFB[1], and 
is designed to be the sole program using the display. 

The NFC system frontend (door-system) needs to be running on the same host. 
