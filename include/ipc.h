/* 
 * File:   ipc.h
 * Author: matt
 *
 * Created on 3 March 2013, 5:06 PM
 */

#ifndef IPC_H
#define	IPC_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef enum {
                CARD_PRESENTED = 1,
                DOOR_OPEN = 2,
                DOOR_ALERT = 3,
                NETWORK_ERROR = 4,
                CARD_DECLINED = 5,
                DOOR_CLOSED = 6,
                SECURITY_BREACH = 7,
                DOOR_OPEN_EXIT = 8,
                NOT_OUR_CARD = 9,
				NOT_CONFIGURED = 10
    } eventId;

    typedef struct {
        eventId event;
        char cardUid[9];
    } ipc_message;


int open_ipc();
int get_next_event(eventId *evtId,char *uid);


#ifdef	__cplusplus
}
#endif

#endif	/* IPC_H */

