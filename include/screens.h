/* 
 * File:   screens.h
 * Author: matt
 *
 * Created on 3 March 2013, 8:51 PM
 */

#ifndef SCREENS_H
#define	SCREENS_H

#ifdef	__cplusplus
extern "C" {
#endif

void setupDirectFB();
void loadFont();
void drawReadyScreen();
void drawOpenScreen();
void drawDeclinedScreen(char *uid);
void drawNetworkError();
void releaseDirectFB();
void drawDoorAlert();
void drawSecurityBreach();
void drawCardNotRecognized();
void drawExitScreen();
void drawNotConfiguredError();
#ifdef	__cplusplus
}
#endif

#endif	/* SCREENS_H */

